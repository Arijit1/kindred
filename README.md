
## README KINDRER WEBUI AUTOMATION


The Web UI testing is done in Java. IntelliJ IDEA is recommended if you run from your local machine.

The following lists down a list of dependencies needed to run the tests locally:

- Java JRE
- Java JDK
- Maven

You also need to have your `JAVA_HOME` and `MAVEN` set up in your machine's environment variables.

You can refer to [here](https://docs.oracle.com/cd/E19182-01/821-0917/inst_jdk_javahome_t/index.html) and [here](https://www.tutorialspoint.com/maven/maven_environment_setup.htm) for instructions on setting up.

Once you have the required dependencies installed, you can execute the tests through the command line interface(CLI).

### Running the tests
To run the tests navigate into the project directory and run the following command
```
mvn test
``` 
A report is generated under the `$projectDir/target/surefire-reports` directory after the tests finish running.
To look at the report you can either open the `$projectDir/target/surefire-reports/index.html` file or the `$projectDir/target/surefire-reports/emailable-report.html`file.

### Tools, Libraries and Frameworks Used

The tests relies on `TestNG` and on `Selenium`. 
`Chromedriver` and `Chrome` is used to simulate the browser environment which loads the website.
`Gitlab` is used as the remote repository.

### Methodology Used

Selenium uses the `chromedriver` webdriver to open the website in the browser. 
Different interactions are then simulated programmatically and assertions are made on expectations.

On any test/assertion failure, a screenshot is taken and appended to the test report.



### POM 
Page object model has been implemented. 

There are 3 Packages/Class in this project.
1. Class Homepage: For this assignment only Homepage is tested. Homepage class has been created which contains all the 
webelements of the homepage.
2. Class Scenario: Test Scenarios are stored in Scenario class.
3. Class Utility/ScreenshotListener: The package utility can be used to store all the utilities such as screenshot recorder/listener,   

### Challenges Faced
1. When the test is run on a small screen, sometimes WebElements can be not interactive. A script has been added for the driver to scroll to the WebElement for interactions. Eg. Class Scenario, Line 57.

### Further Improvements 

1. Add capability to read from files. Utility such as Apache POI can be expored to acheive this.
2. Implement BDD framework such as Cucumber
3. Implement CI