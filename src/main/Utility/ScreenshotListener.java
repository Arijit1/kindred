package main.Utility;

import main.Scnenario_.Scenario;
import main.Scnenario_.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScreenshotListener extends TestListenerAdapter {
    @Override
    public void onTestFailure(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            captureScreenshot(Scenario.driver, result.getName());
        }
    }
    public void captureScreenshot(WebDriver driver, String screenshotName) {

        try {
            TakesScreenshot ts = (TakesScreenshot) driver;
            File source = ts.getScreenshotAs(OutputType.FILE);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

            String reportDirectory = new File(System.getProperty("user.dir")).getAbsolutePath() + "/target/surefire-reports";
            String relPath = "/failure_screenshots/" + screenshotName + "_" + formater.format(calendar.getTime()) + ".png";
            File destFile = new File(reportDirectory + relPath);
            FileUtils.copyFile(source, destFile);
            Reporter.log("<a href='./" +relPath + "'> <img src='./" + relPath + "' height='100' width='100'/> </a>");

        } catch (Exception e) {

            System.out.println("Exception while taking screenshot " + e.getMessage());
        }
    }



}
