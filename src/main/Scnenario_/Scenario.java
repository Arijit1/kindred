package main.Scnenario_;

import main.PagesAndWebelements.Homepage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Scenario {
    public static WebDriver driver;

    @BeforeTest
    public void setup() {
        String os = System.getProperty("os.name");

        ChromeOptions options = new ChromeOptions().setHeadless(false);
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        System.setProperty("webdriver.chrome.driver", "chromedriver92.exe");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);// waits for elements to load
        driver.manage().window().maximize(); // maximize the browser window


    }

    @Test(priority = 1)
    public void navigateToHomepage() {
        // load url

        driver.get("https://www.unibet.com.au/racing#/lobby/all");
        // Wait for the page to load
        //Reference: https://stackoverflow.com/questions/36590274/selenium-how-to-wait-until-page-is-completely-loaded
        new WebDriverWait(driver, 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

    }

    @Test(priority = 2)
    public void clickNTG_F() throws InterruptedException {
        //read the number of rows in Next To Go Table
        Homepage homepage = new Homepage(driver);
        List<WebElement> table1 = (homepage.getTable());


        int noOfRows = table1.size();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        homepage.scrollToEndOfTable_Element_F(noOfRows);
        boolean elementWithF_Found = false;
        while (!elementWithF_Found) {
            try {

                homepage.clickF(noOfRows);
                elementWithF_Found = true;
                System.out.println("The element F is found in row " + noOfRows + ".");
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            } catch (Exception e) {
                noOfRows--;
            }
        }
    }

    @Test(priority = 3)
    public void clickLowestFixedPrice() throws InterruptedException {
        Homepage homepage = new Homepage(driver);
        homepage.clickSort();// sort price in decending order
        homepage.clickLowestPrice(); // click lowest price
    }

    @Test(priority = 4)
    public void validateBetSlip() {
        Homepage homepage = new Homepage(driver);
//        a.	Event Name
        Assert.assertNotNull(homepage.getEventname().getText());

//        b.	Event type icon
        Assert.assertNotNull(homepage.getEvenTypeIcon().getSize());


        //        c.	Price
        Assert.assertNotNull(homepage.getPrice().getSize());


//        d.	Runner sequence
//        e.	Runner name
        Assert.assertNotNull(homepage.getRunnerSeqName().getSize());


//        f.	Market. eg Fixed (Win)
        Assert.assertNotNull(homepage.getMarket().getText());
    }


    @AfterTest
    public void exit() {
        driver.quit();
    }
}
