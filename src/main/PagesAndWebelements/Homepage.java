package main.PagesAndWebelements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Homepage {
    public Homepage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    //WebElements
    private static By table = By.xpath("//*/div[@data-test-id= 'ntg' ]/div/div/a[@data-test-id='ntg-sequence-event-item-link']");
    private static By sortLocator = By.xpath("//*/button[@data-test-id='event-card-sortby-Price']/span[contains(text(),'FIXED')]");
    private static By lowestPriceLocator = By.xpath("//*/button[@class= 'sc-fvNhHS jcWxmD  ']");
    private static By eventNameLocator = By.xpath("//*/div[@data-test-id=\"bet-card-race-title\"]");
    private static By evenTypeIconLocator = By.cssSelector("#betslip > div.sc-kHVIJG.fKcWPA > div.sc-bdnxRM.fzCJcj > div > div.betCardHeader > div > div.sc-bCwfaz.hzzSzX > svg > path");
    private static By priceLocator = By.xpath("//*[@id='betslip']/div[2]/div[1]/div/div[4]/section/span[2]");
    private static By runnerSeqName = By.xpath("//*[@id='betslip']/div[2]/div[1]/div/div[2]/div");
    private static By marketLocator = By.xpath("//*[@id=\"betslip\"]/div[2]/div[1]/div/div[4]/section/span[2]");


    public List<WebElement> getTable() {
        return this.driver.findElements(table);
    }

    public void scrollToEndOfTable_Element_F(int noOfRows) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.cssSelector("div[data-test-id='ntg']>div>div>a:nth-child(" + noOfRows + ")>button>span>div>svg")));
    }

    public void clickF(int noOfRows) {
        driver.findElement(By.cssSelector("div[data-test-id='ntg']>div>div>a:nth-child(" + noOfRows + ")>button>span>div>svg")).click();
    }

    //Getters and Setters for sortLocator
    private WebElement getSortLocator() {
        return this.driver.findElement(sortLocator);
    }

    public void clickSort() {
        this.getSortLocator().click();
    }

    // Getters and Setters for lowesPirceLocator
    private WebElement getLowestPrice() {
        return this.driver.findElement(lowestPriceLocator);
    }

    public void clickLowestPrice() {
        this.getLowestPrice().click();
    }

    //Getter for event name
    public WebElement getEventname() {
        return this.driver.findElement(eventNameLocator);
    }

    //Getter for evenTypeIcon
    public WebElement getEvenTypeIcon() {
        return this.driver.findElement(evenTypeIconLocator);
    }

    // Getter for priceLocator
    public WebElement getPrice() {
        return this.driver.findElement(priceLocator);
    }

    //Getter for Runner Sequence and Name
    public WebElement getRunnerSeqName() {
        return this.driver.findElement(runnerSeqName);
    }

    // Getter for Market
    public WebElement getMarket() {
        return this.driver.findElement(marketLocator);
    }
}

